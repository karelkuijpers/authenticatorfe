<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

$EM_CONF[$_EXTKEY] = [
    'title'            => 'Authenticatorfe',
    'description'      => 'Implements a two factor frontend authentication for TYPO3 via TOTP.',
    'category'         => 'services',
    'author'           => 'Philipp Gampe',
    'author_email'     => 'philipp.gampe@typo3.org',
    'author_company'   => '',
    'state'            => 'stable',
    'uploadfolder'     => 0,
    'createDirs'       => '',
    'clearCacheOnLoad' => 1,
    'version'          => '11.0.3',
    'constraints'      => [
        'depends' => [
            'typo3' => '^10.4.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests'  => [],
    ],
];
